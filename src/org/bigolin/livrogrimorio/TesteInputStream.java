/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bigolin.livrogrimorio;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import org.bigolin.livrogrimorio.model.Magia;

/**
 *
 * @author developer
 */
public class TesteInputStream {

    public static void main(String... args) {

        try {
            FileInputStream fos = new FileInputStream("pocoes.dat");
            ObjectInputStream ois = new ObjectInputStream(fos);
            Magia p = (Magia) ois.readObject();
            System.out.println(p);
            
            fos.close();
            ois.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
}
