/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bigolin.livrogrimorio.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author gamificacao
 */
public class Magia implements Serializable{
    private int idMagia;
    private Date dataCriacao;
    private String titulo;
    private List<Ingrediente> ing = new ArrayList<>();
    public int getIdMagia() {
        return idMagia;
    }

    public void setIdMagia(int idMagia) {
        this.idMagia = idMagia;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<Ingrediente> getAllIngredientes() {
        return ing;
    }

    
    public void addIngrediente(Ingrediente ing){
        this.ing.add(ing);
    }

    @Override
    public String toString() {
        return "Magia{" + "idMagia=" + idMagia + ", dataCriacao=" + dataCriacao + ", titulo=" + titulo + ", ing=" + ing + '}';
    }
    
    
    
}
